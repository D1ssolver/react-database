import React, { Component } from 'react';
import axios from 'axios'; // Импортируем Axios

import DataForm from './DataForm'; // Импортируем компонент DataForm

class App extends Component {
  handleSubmit = (data) => {
    // Отправляем GET-запрос на сервер с помощью Axios
    axios
      .get('http://127.0.0.1:8080/statistics', { params: data }) // Замените на реальный URL вашего бэкенд-сервера
      .then((response) => {
        if (response.status === 200) {
          console.log('Запрос успешно выполнен.');

          // Обработка ответа от сервера (зависит от вашей бизнес-логики)
          if (response.data && response.data.success) {
            // Если сервер вернул успешный ответ, вы можете выполнить определенные действия
            console.log('Сервер вернул успешный ответ:', response.data.message);

            // Здесь вы можете обновить состояние вашего компонента или выполнить другие действия
          } else {
            console.error('Сервер вернул ошибку:', response.data.error);
          }
        } else {
          console.error('Произошла ошибка при выполнении запроса.');
        }
      })
      .catch((error) => {
        console.error('Произошла ошибка при выполнении запроса:', error);
      });
  };

  render() {
    return (
      <div>
        <h1>Введите данные:</h1>
        {/* Передаем функцию handleSubmit компоненту DataForm */}
        <DataForm onSubmit={this.handleSubmit} />
      </div>
    );
  }
}

export default App;

// http://127.0.0.1:8080/statistics
