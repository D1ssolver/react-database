import React, { Component } from 'react';

class DataForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: '',
      user: '',
      device: '',
      amount: '',
      id: '',
    };
  }

  handleInputChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const data = {
      type: this.state.type,
      user: this.state.user,
      device: this.state.device,
      amount: this.state.amount,
      id: this.state.id,
    };

    // Вызываем функцию onSubmit, переданную из родительского компонента (App)
    this.props.onSubmit(data);

    // Опционально можно сбросить значения полей после отправки
    this.setState({
      type: '',
      user: '',
      device: '',
      amount: '',
      id: '',
    });
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <label htmlFor="type">Type:</label>
          <input
            type="text"
            id="type"
            name="type"
            value={this.state.type}
            onChange={this.handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="user">User:</label>
          <input
            type="text"
            id="user"
            name="user"
            value={this.state.user}
            onChange={this.handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="device">Device:</label>
          <input
            type="text"
            id="device"
            name="device"
            value={this.state.device}
            onChange={this.handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="amount">Amount:</label>
          <input
            type="number"
            id="amount"
            name="amount"
            value={this.state.amount}
            onChange={this.handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="id">Id:</label>
          <input
            type="text"
            id="id"
            name="id"
            value={this.state.id}
            onChange={this.handleInputChange}
          />
        </div>
        <div>
          <button type="submit">Отправить</button>
        </div>
      </form>
    );
  }
}

export default DataForm;
